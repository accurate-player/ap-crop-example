import React, { Component } from "react";
import "./App.css";
import { getMediaInfo } from "@accurate-player/probe";
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import {HotkeyPlugin, SmoothTimeUpdatePlugin} from "@accurate-player/accurate-player-plugins";
import "@accurate-player/accurate-player-controls";

const SINTEL = {
  file: "https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/timecode/sintel-2048-timecode-stereo.mp4",
  width: 2048,
  height: 872,
}

const BUNNY = {
  file: "https://accurate-player-demo-assets.s3.eu-central-1.amazonaws.com/original/Bunny_H264_Webproxy_TC2.mp4",
  width: 512,
  height: 288,
}

const VIDEO = SINTEL;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cropTop: 0,
      cropBottom: 0,
      cropLeft: 0,
      cropRight: 0
    };
  }

  videoElement;
  apControls;
  player;
  topCropLine;
  leftCropLine;
  rightCropLine;
  bottomCropLine;
  videoContainer;

  // Dimensions ratio constant for video, original vs shown on screen
  ratio = 1;

  componentDidMount() {
    this.player = new ProgressivePlayer(this.videoElement, window.LICENSE_KEY);
    new SmoothTimeUpdatePlugin(this.player);
    new HotkeyPlugin(this.player);

    getMediaInfo(VIDEO.file, {label: "Accurate Player Cropping Example", dropFrame: false}).then(file => {
      this.player.api.loadVideoFile(file);
    });

    this.setHotkeys();
    this.initControls();

    // Reset crop values
    this.setCropTop(0);
    this.setCropLeft(0);
    this.setCropRight(0);
    this.setCropBottom(0);

    this.videoElement.addEventListener('loadedmetadata', () => {
      this.calculateVideoDimensions();
    });
  }

  calculateVideoDimensions() {
    let width = VIDEO.width;
    let height = VIDEO.height;

    console.debug('Finding window width');

    // Scale down
    while (width > window.innerWidth) {
      console.debug('Width is higher than window width', width, window.innerWidth);
      this.ratio *= 2;
      width /= this.ratio;
    }

    height /= this.ratio;

    console.debug('Width', width);
    console.debug('Height', height);
    console.debug('Ratio', this.ratio);

    this.videoContainer.setAttribute("style", "width: "+ width + "px; height:" + height + "px;");
  }

  setHotkeys() {
    this.player.api.plugin.apHotkeyPlugin.setHotkey("q", () => {
      this.setCropTop(this.state.cropTop + 1);
    }, "Increase crop top");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("w", () => {
      this.setCropTop(this.state.cropTop - 1);
    }, "Decrease crop top");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("a", () => {
      this.setCropLeft(this.state.cropLeft + 1);
    }, "Increase crop left");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("s", () => {
      this.setCropLeft(this.state.cropLeft - 1);
    }, "Decrease crop left");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("z", () => {
      this.setCropRight(this.state.cropRight + 1);
    }, "Increase crop right");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("x", () => {
      this.setCropRight(this.state.cropRight - 1);
    }, "Decrease crop right");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("e", () => {
      this.setCropBottom(this.state.cropBottom + 1);
    }, "Increase crop bottom");

    this.player.api.plugin.apHotkeyPlugin.setHotkey("r", () => {
      this.setCropBottom(this.state.cropBottom - 1);
    }, "Decrease crop bottom");
  }

  initControls() {
    // Check if method exist, otherwise wait 100ms and try again.
    if (!this.apControls.init) {
      setTimeout(() => {
        this.initControls();
      }, 100);
      return;
    }

    this.apControls.init(this.videoElement, this.player, {
      saveLocalSettings: true
    });
  }

  setCropTop(cropTop) {
    if (cropTop < 0) {
      cropTop = 0;
    }

    this.setState({ cropTop: cropTop }, () => {
      this.updateCropLines();
    })
  }

  setCropLeft(cropLeft) {
    if (cropLeft < 0) {
      cropLeft = 0;
    }

    this.setState({ cropLeft: cropLeft }, () => {
      this.updateCropLines();
    })
  }

  setCropRight(cropRight) {
    if (cropRight < 0) {
      cropRight = 0;
    }

    this.setState({ cropRight: cropRight }, () => {
      this.updateCropLines();
    })
  }

  setCropBottom(cropBottom) {
    if (cropBottom < 0) {
      cropBottom = 0;
    }

    this.setState({ cropBottom: cropBottom }, () => {
      this.updateCropLines();
    })
  }

  updateCropLines() {
    let cropTopAdjusted = this.state.cropTop / this.ratio;
    let cropBottomAdjusted = this.state.cropBottom / this.ratio;
    let cropLeftAdjusted = this.state.cropLeft / this.ratio;
    let cropRightAdjusted = this.state.cropRight / this.ratio;

    console.debug('Crop values adjusted', cropTopAdjusted, cropBottomAdjusted, cropLeftAdjusted, cropRightAdjusted);

    this.topCropLine.setAttribute("style", "top: "+ cropTopAdjusted + "px;" +
      "visibility:" + (this.state.cropTop === 0 ? "hidden" : "visible"));

    this.leftCropLine.setAttribute("style", "left: "+ cropLeftAdjusted + "px;" +
      "visibility:" + (this.state.cropLeft === 0 ? "hidden" : "visible"));

    this.rightCropLine.setAttribute("style", "right: "+ cropRightAdjusted + "px;" +
      "visibility:" + (this.state.cropRight === 0 ? "hidden" : "visible"));

    this.bottomCropLine.setAttribute("style", "bottom: "+ cropBottomAdjusted + "px;" +
      "visibility:" + (this.state.cropBottom === 0 ? "hidden" : "visible"));
  }

  render() {
    return (
      <div className="App">
        <div
          ref={ref => (this.videoContainer = ref)}
          className="video-container">
          <video
            ref={ref => (this.videoElement = ref)}
            crossOrigin="true"
            playsInline={true}/>
          <apc-controls
            ref={ref => (this.apControls = ref)}/>
          <div className="overlay">
            <div className="red-line horizontal-line" ref={ref => (this.topCropLine = ref)}></div>
            <div className="red-line vertical-line" ref={ref => (this.leftCropLine = ref)}></div>
            <div className="red-line vertical-line" ref={ref => (this.rightCropLine = ref)}></div>
            <div className="red-line horizontal-line" ref={ref => (this.bottomCropLine = ref)}></div>
          </div>
        </div>

        <div className="button-container">
          <div>
            <span className="label">Crop top</span><span>{this.state.cropTop}</span>
            <button onClick={() => this.setCropTop(this.state.cropTop + 1)}>Increase (q)</button>
            <button onClick={() => this.setCropTop(this.state.cropTop - 1)}>Decrease (w)</button>
            <button onClick={() => this.setCropTop(0)}>Clear</button>
          </div>

          <div>
            <span className="label">Crop left</span><span>{this.state.cropLeft}</span>
            <button onClick={() => this.setCropLeft(this.state.cropLeft + 1)}>Increase (a)</button>
            <button onClick={() => this.setCropLeft(this.state.cropLeft - 1)}>Decrease (s)</button>
            <button onClick={() => this.setCropLeft(0)}>Clear</button>
          </div>

          <div>
            <span className="label">Crop right</span><span>{this.state.cropRight}</span>
            <button onClick={() => this.setCropRight(this.state.cropRight + 1)}>Increase (z)</button>
            <button onClick={() => this.setCropRight(this.state.cropRight - 1)}>Decrease (x)</button>
            <button onClick={() => this.setCropRight(0)}>Clear</button>
          </div>

          <div>
            <span className="label">Crop bottom</span><span>{this.state.cropBottom}</span>
            <button onClick={() => this.setCropBottom(this.state.cropBottom + 1)}>Increase (e)</button>
            <button onClick={() => this.setCropBottom(this.state.cropBottom - 1)}>Decrease (r)</button>
            <button onClick={() => this.setCropBottom(0)}>Clear</button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
