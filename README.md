# AP Crop Example

This repo shows an example of how to use overlays to indicate crop sections in Accurate Player. Use the buttons below
the video to move the crop overlays, or use the hotkeys within parenthesis.

## Demo

![Crop overlay example](app.png)

Demo on the link below:

[https://demo.accurateplayer.com/crop-example/](https://demo.accurateplayer.com/crop-example/)

## Usage

You can use this to set four metadata fields which are fed into a ffmpeg transcode to crop the video. The ffmpeg
command to use looks like the following:

```
ffmpeg -i input.mp4 -filter:v "crop=w:h:x:y" output.mp4
```

The four crop values are defined as follows:

* w - the width of the output video (so the width of the cropped region), which defaults to the input video width
  (input video width = `iw`)
* h - the height of the output video (the height of the cropped region), which defaults to the input video height
  (input video height = `ih`)
* x - the horizontal position from where to begin cropping, starting from the left (with the absolute left margin being 0)
* y - the vertical position from where to begin cropping, starting from the top of the video (the absolute top being 0)

Example:

![Before cropped](before-cropped.png)

Looking at the above example, to crop away the black bars top and bottom, and also cut 100px from left and right. So
we have the following variables:

```
cropTop = 83
cropLeft = 100
cropRight = 100
cropBottom = 83
```
Replace the ffmpeg command line according to the following:

```
w = iw - cropLeft - cropRight
h = ih - cropTop - cropBottom
x = cropLeft
y = cropTop
```

Final command:

```
ffmpeg -i input.mp4 -filter:v "crop=iw-200:ih-166:100:83" output.mp4
```

## Setup

To setup the demos execute the following:

1. Login to Accurate Player npm repository to gain access to required dependencies, username and password will be provided to you by the Accurate Player team.
> npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player

2. Go to `shared/license-key-example.js` and follow the instructions of how to provide your license key correctly.

## Running the demo

1. `npm install`
2. `npm run start`
3. Open `http://localhost:3000` in your browser.
